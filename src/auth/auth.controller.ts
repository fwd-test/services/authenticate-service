import { Body, Controller, Post,Res,HttpStatus } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthDto } from './dto/auth.dto';
import { AxiosResponse } from 'axios';
import { AuthService } from './auth.service';
import { Response } from 'express';
import { UsersService } from '../users/users.service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService,private userService: UsersService) {}

  @Post()
  async auth(@Body() authDto: AuthDto, @Res() res: Response): Promise<Response>{
    const user  = await this.userService.getUserByUsername(authDto.username);
    if(!user){
      return res.status(HttpStatus.BAD_REQUEST).json({'msg':'User not found'});
    }
    const auth = await this.authService.validateUser(user,authDto.password);
    if(!auth){
      return res.status(HttpStatus.BAD_REQUEST).json({'msg': 'Username or password incorrect.'});
    }else{
      const jwt = await this.authService.login(user);
      return res.status(HttpStatus.OK).json(jwt);
    }
  }
}
