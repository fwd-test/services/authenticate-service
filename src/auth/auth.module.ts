import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from '../users/users.module';
import {ConfigModule} from '@nestjs/config';
import configuration from '../config/configuration';
import { JwtModule } from '@nestjs/jwt';
import {jwtConstants} from './constants';

@Module({
  imports:[
    UsersModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60s' },
    }),
    ConfigModule.forRoot({
      load: [configuration],
    }),
  ],
  providers: [AuthService],
  controllers: [AuthController]
})
export class AuthModule {}
