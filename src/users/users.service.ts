import { Inject, Injectable } from '@nestjs/common';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @Inject('USERS_REPOSITORY')
    private usersRepository: typeof User
  ) {}

  async getUserByUsername(username:string): Promise<User | undefined>{
    return await this.usersRepository.findOne<User>({where:{username}});
  }
}
