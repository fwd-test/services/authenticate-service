import { Table, Column, Model,CreatedAt,UpdatedAt } from 'sequelize-typescript';

@Table
export class User extends Model {
  @Column({
    primaryKey:true,
    autoIncrement:true
  })
  id:number;

  @Column
  username: string;

  @Column
  password: string;

  @Column
  salt:string;

  @CreatedAt
  creationDate: Date;

  @UpdatedAt
  updatedOn: Date;
}