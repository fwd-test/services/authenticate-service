import { DatabaseModule } from '../database.module';
import { UsersSeederModule } from './user/user.module';
import { Logger,Module } from '@nestjs/common';
import {Seeder} from './seeder';
import {DatabaseProviders} from '../database.providers';

@Module({
  imports: [DatabaseModule, UsersSeederModule],
  providers: [...DatabaseProviders, Logger, Seeder],
})
export class SeederModule {}