import { DatabaseModule } from '../../database.module';
import {Module} from '@nestjs/common';
import {UsersSeederService} from './user.service';
import { usersProviders } from '../../../users/users.providers';

@Module({
  imports: [DatabaseModule],
  providers: [UsersSeederService,...usersProviders],
  exports: [UsersSeederService],
})
export class UsersSeederModule {}