import { Inject, Injectable } from '@nestjs/common';
import { User } from '../../../users/user.entity';
import { IUser } from './users.interface';
import {users} from './data';
import * as bcrypt from 'bcrypt';
@Injectable()
export class UsersSeederService {
  constructor(
    @Inject('USERS_REPOSITORY')
    private usersSeederRepository: typeof User
  ) {}
  create(): Array<Promise<User>> {
    return users.map(async (user: IUser) => {
      return await this.usersSeederRepository
        .findOne<User>({where: {username:user.username}})
        .then(async dbUser => {
          if (dbUser) {
            return Promise.resolve(null);
          }
          const salt = await bcrypt.genSalt();
          const hash = await bcrypt.hash(user.password, salt);
          const body = {
            username:user.username,
            password:hash,
            salt:salt
          }
          console.log(body);
          return Promise.resolve(
            await this.usersSeederRepository.create(body),
          );
        })
        .catch(error => Promise.reject(error));
    });
  }
}